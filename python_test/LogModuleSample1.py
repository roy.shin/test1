import logging

logging.basicConfig(level=logging.DEBUG)

logging.debug('DEBUG message')
logging.info('INFO message')
logging.warning('WARNING message')
logging.error('ERROR message')
logging.critical('CRITICAL message')

if __name__ == '__main__':
    print('LogModuleSample1 is running')
else:
    print('LogModuleSample1 is imported')
